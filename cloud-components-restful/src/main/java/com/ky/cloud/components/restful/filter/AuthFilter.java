package com.ky.cloud.components.restful.filter;


import com.alibaba.fastjson2.JSON;
import com.ky.cloud.components.auth.AuthUtils;
import com.ky.cloud.components.auth.model.LoginUser;
import jakarta.annotation.Resource;
import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import com.ky.cloud.components.context.SystemContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * http请求权限校验过滤器
 *
 * @author chenHui
 * @version 1.0.0
 */
@Slf4j
@WebFilter(filterName = "authFilter", urlPatterns = "/*")
@Configuration
@Order(2)
public class AuthFilter implements Filter {

    @Value("${spring.profiles.active}")
    private String env;

    @Resource
    private AuthUtils authUtils;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        String token = httpServletRequest.getHeader("Authorization");
        if (!StringUtils.hasText(token)) {
            String path = httpServletRequest.getRequestURI().substring(httpServletRequest.
                    getContextPath().length());
            if (!authUtils.isNeedAuth(path)){
                chain.doFilter(request, response);
                return;
            }
            if ("dev".equals(env)) {
                // 本地开发环境放行
                SystemContext.addOperator("dev");
                chain.doFilter(request, response);
                return;
            }
            log.info("请求路径{},未获取到请求token", path);
            noAuthResp(httpServletResponse);
            return;
        }
        LoginUser loginUser = authUtils.getLoginUser(token);
        if (loginUser == null) {
            noAuthResp(httpServletResponse);
            return;
        }
        authUtils.addUserToContext(loginUser);
        chain.doFilter(request, response);
    }

    private void noAuthResp(HttpServletResponse httpServletResponse) throws IOException {
        httpServletResponse.setStatus(200);
        httpServletResponse.setCharacterEncoding("UTF-8");
        httpServletResponse.setContentType("text/html; charset=utf-8");
        Map<String, String> map = new HashMap<>();
        map.put("code", "401");
        map.put("msg", "Permission denied");
        httpServletResponse.getWriter().write(JSON.toJSONString(map));
    }
}

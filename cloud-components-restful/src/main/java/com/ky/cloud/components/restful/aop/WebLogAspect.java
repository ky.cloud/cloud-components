package com.ky.cloud.components.restful.aop;

import com.alibaba.fastjson2.JSON;
import com.ky.cloud.components.restful.utils.WebUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.MethodParameter;
import org.springframework.core.ParameterNameDiscoverer;
import org.springframework.core.annotation.SynthesizingMethodParameter;
import org.springframework.core.io.InputStreamSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * restful日志切面(代码参考springBlade)
 *
 * @author chenHui
 * @version 1.0.0
 */
@Slf4j
@Aspect
@Component
public class WebLogAspect {
	private static final ParameterNameDiscoverer PARAMETER_NAME_DISCOVERER = new DefaultParameterNameDiscoverer();
	@Around("execution(public * com..controller..*.*(..))")
	public Object invoke(ProceedingJoinPoint point) throws Throwable {
		HttpServletRequest request = WebUtil.getRequest();
		String requestURI = (Objects.requireNonNull(request)).getRequestURI();
		String requestMethod = request.getMethod();
		StringBuilder beforeReqLog = new StringBuilder(300);
		List<Object> beforeReqArgs = new ArrayList<>();
		beforeReqLog.append("\n=======  Request Start  ================\n");
		beforeReqLog.append("===> {}: {}\n");
		beforeReqArgs.add(requestMethod);
		beforeReqArgs.add(requestURI);

		Map<String, Object> paraMap = getRequestParameter(point);
		if (!paraMap.isEmpty()) {
			beforeReqLog.append("===> Parameters: {}\n");
			beforeReqArgs.add(JSON.toJSONString(paraMap));
		}

		beforeReqLog.append("================  Request End   ================\n");

		long startNs = System.nanoTime();
		log.info(beforeReqLog.toString(), beforeReqArgs.toArray());
		StringBuilder afterReqLog = new StringBuilder(200);
		afterReqLog.append("\n\n================  Response Start  ================\n");
		afterReqLog.append("<=== Parameters: {} \n");

		List<Object> afterReqArgs = new ArrayList<>();


		boolean flag = false;

		Object o;
		try {
			flag = true;
			Object result = point.proceed();
			afterReqLog.append("<=== cost:  {} ms\n");
			afterReqArgs.add(JSON.toJSONString(result));
			o = result;
			flag = false;
		} finally {
			if (flag) {
				long cost = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startNs);
				afterReqArgs.add(cost);
				afterReqLog.append("================  Response End   ================\n");
				log.info(afterReqLog.toString(), afterReqArgs.toArray());
			}
		}

		long cost = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startNs);
		afterReqArgs.add(cost);
		afterReqLog.append("================  Response End   ================\n");
		log.info(afterReqLog.toString(), afterReqArgs.toArray());
		return o;
	}

	/**
	 * 获取请求参数
	 *
	 * @param point ProceedingJoinPoint
	 * @return Map<String, Object> 请求参数
	 * @since 1.0.0
	 */
	public Map<String, Object> getRequestParameter(ProceedingJoinPoint point){
		MethodSignature ms = (MethodSignature)point.getSignature();
		Method method = ms.getMethod();
		Object[] args = point.getArgs();
		Map<String, Object> paraMap = new HashMap<>(16);
		String paraName;
		for(int i = 0; i < args.length; ++i) {
			MethodParameter methodParam = new SynthesizingMethodParameter(method, i);
			methodParam.initParameterNameDiscovery(PARAMETER_NAME_DISCOVERER);

			PathVariable pathVariable = methodParam.getParameterAnnotation(PathVariable.class);
			if (pathVariable == null){
				RequestBody requestBody = methodParam.getParameterAnnotation(RequestBody.class);
				String parameterName = methodParam.getParameterName();
				Object value = args[i];
				if (requestBody != null && value != null) {
					paraMap.putAll(BeanMap.create(value));
				} else {
					if (value instanceof List) {
						value = ((List<?>)value).get(0);
					}

					if (value instanceof HttpServletRequest) {
						paraMap.putAll(((HttpServletRequest)value).getParameterMap());
					} else if (value instanceof WebRequest) {
						paraMap.putAll(((WebRequest)value).getParameterMap());
					} else if (value instanceof MultipartFile multipartFile) {
						paraName = multipartFile.getName();
						String fileName = multipartFile.getOriginalFilename();
						paraMap.put(paraName, fileName);
					} else if (!(value instanceof HttpServletResponse) && !(value instanceof InputStream) && !(value instanceof InputStreamSource)) {
						if (value instanceof List<?> list) {
							AtomicBoolean isSkip = new AtomicBoolean(false);
							for (Object o : list) {
								if ("StandardMultipartFile".equalsIgnoreCase(o.getClass().getSimpleName())) {
									isSkip.set(true);
									break;
								}
							}

							if (isSkip.get()) {
								paraMap.put(parameterName, "此参数不能序列化为json");
							}
						} else {
							RequestParam requestParam = methodParam.getParameterAnnotation(RequestParam.class);
							if (requestParam != null && !StringUtils.hasText(requestParam.value())) {
								paraName = requestParam.value();
							} else {
								paraName = methodParam.getParameterName();
							}

							paraMap.put(paraName, value);
						}
					}
				}
			}
		}
		return paraMap;
	}
}

package com.ky.cloud.components.restful.exception;

/**
 * 业务异常
 *
 * @author chenHui
 * @version 1.0.0
 */
public class BusinessException extends RuntimeException {
    public BusinessException() {
        super();
    }

    public BusinessException(String message) {
        super(message);
    }
}

package com.ky.cloud.components.restful.model;

import java.io.Serializable;

/**
 * 分页查询入参
 *
 * @author chenHui
 * @version 1.0.0
 */
public class PageQuery implements Serializable {
    /**
     * 第几页
     */
    private Integer pageIndex;

    /**
     * 每页数量
     */
    private Integer pageSize;

    public Integer getPageIndex() {
        if (pageIndex != null) {
            return pageIndex;
        }
        return 1;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize() {
        if (pageSize!=null){
            return pageSize;
        }
        return 10;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}

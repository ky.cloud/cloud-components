package com.ky.cloud.components.restful.model;



import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 数据枚举
 *
 * @author chenHui
 * @version 1.0
 */
@Data
public class DataEnum implements Serializable {
    /**
     * 编码
     */
    private String value;
    /**
     * 名称
     */
    private String label;

    /**
     * 默认标志
     */
    private String isDefault;

    /**
     * 页面是否显示
     */
    private String isShow;

    /**
     * 子数据类型
     */
    private List<DataEnum> children;
}

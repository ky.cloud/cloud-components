package com.ky.cloud.components.restful;


import java.io.Serializable;

/**
 * restful统一返回出参
 *
 * @author chenHui
 * @version 1.0.0
 */
public class HttpResult implements Serializable {
    /**
     * 响应状态码
     */
    private Integer code;

    /**
     * 错误信息
     */
    private String msg;

    /**
     * 响应数据
     */
    private Object data;

    /**
     * 请求成功返回
     *
     * @return HttpResult
     * @since 1.0.0
     */
    public static HttpResult success() {
        HttpResult result = new HttpResult();
        result.code = 1;
        result.msg = "成功";
        return result;
    }
    /**
     * 请求成功返回
     *
     * @param data 返回数据
     * @return HttpResult
     * @since 1.0.0
     */
    public static HttpResult success(Object data) {
        HttpResult result = new HttpResult();
        result.data = data;
        result.code = 1;
        result.msg = "成功";
        return result;
    }

    /**
     * 请求失败返回
     *
     * @param msg 错误信息
     * @return HttpResult
     * @since 1.0.0
     */
    public static HttpResult failure(String msg) {
        HttpResult result = new HttpResult();
        result.code = 0;
        result.msg = msg;
        return result;
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}

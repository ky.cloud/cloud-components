package com.ky.cloud.components.restful.filter;

import com.ky.cloud.components.context.SystemContext;
import com.ky.cloud.components.context.constant.TraceConstant;
import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.MDC;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.util.StringUtils;

import java.io.IOException;

/**
 * webTrace过滤器
 *
 * @author chenHui
 * @version 1.0.0
 */
@Order(1)
@Configuration
@WebFilter(filterName = "webTraceFilter",urlPatterns = "/*")
public class WebTraceFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        try {
            HttpServletRequest request = (HttpServletRequest) servletRequest;
            HttpServletResponse response = (HttpServletResponse) servletResponse;
            String traceId = request.getHeader(TraceConstant.MDC_TRACE_ID);

            if (!StringUtils.hasText(traceId)) {
                SystemContext.initTraceMdc();
                traceId = SystemContext.getContext().getTraceId();
            } else {
                String spanId = request.getHeader(TraceConstant.MDC_SPAN_ID);
                SystemContext.nextSpan(spanId);
                SystemContext.initTraceMdc();
            }
            response.setHeader(TraceConstant.MDC_TRACE_ID, traceId);

            filterChain.doFilter(request, response);
        }finally {
            MDC.clear();
            SystemContext.remove();
        }
    }

    @Override
    public void init(FilterConfig filterConfig) {
        // 无操作
    }

    @Override
    public void destroy() {
        // 无操作
    }
}

package com.ky.cloud.components.restful.utils;


import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.WebUtils;

import java.net.InetAddress;

/**
 * web工具类(参考SpringBlade)
 *
 * @author chenHui
 * @version 1.0.0
 */
@Slf4j
public class WebUtil extends WebUtils {

    private static final String UNKNOWN = "unknown";
    private static final String LOCAL_IP = "0:0:0:0:0:0:0:1";
    private static final String LOCAL_IP1 = "127.0.0.1";



    /**
     * 获取requestAttributes
     *
     * @return requestAttributes
     * @since 1.0.0
     */
    public static HttpServletRequest getRequest() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        return requestAttributes == null ? null : ((ServletRequestAttributes)requestAttributes).getRequest();
    }

    /**
     * 获取请求ip
     *
     * @return ip
     * @since 1.0.0
     */
    public static String getRequestIp(){
        HttpServletRequest request = getRequest();
        if (request == null){
            return null;
        }
        String ipAddress = null;
        try {
            // 获取k8s中
            ipAddress = request.getHeader("x-Original-Forwarded-for");
            if (!StringUtils.hasText(ipAddress) || UNKNOWN.equalsIgnoreCase(ipAddress)) {
                ipAddress = request.getHeader("x-Forwarded-For");
            }
            // 获取nginx等代理ip
            if (!StringUtils.hasText(ipAddress) || UNKNOWN.equalsIgnoreCase(ipAddress)) {
                ipAddress = request.getHeader("x-forwarded-for");
            }
            if (!StringUtils.hasText(ipAddress) || UNKNOWN.equalsIgnoreCase(ipAddress)) {
                ipAddress = request.getHeader("Proxy-Client-IP");
            }
            if (!StringUtils.hasText(ipAddress) || UNKNOWN.equalsIgnoreCase(ipAddress)) {
                ipAddress = request.getHeader("WL-Proxy-Client-IP");
            }
            if (!StringUtils.hasText(ipAddress) || UNKNOWN.equalsIgnoreCase(ipAddress)) {
                ipAddress = request.getHeader("HTTP-Client-IP");
            }
            if (!StringUtils.hasText(ipAddress) || UNKNOWN.equalsIgnoreCase(ipAddress)) {
                ipAddress = request.getHeader("HTTP-X_FORWARDED_FOR");
            }
            // 兼容k8s集群获取ip
            if (!StringUtils.hasText(ipAddress) || UNKNOWN.equalsIgnoreCase(ipAddress)) {
                ipAddress = request.getRemoteAddr();
                if (LOCAL_IP.equals(ipAddress) || LOCAL_IP1.equals(ipAddress)) {
                    // 根据网卡取本机配置的IP
                    InetAddress inet = InetAddress.getLocalHost();
                    if (inet != null) {
                        ipAddress = inet.getHostAddress();
                    }
                }
            }
        } catch (Exception e) {
            log.error("获取请求ip异常",e);
        }
        // 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
        if (StringUtils.hasText(ipAddress) && ipAddress.indexOf(",") > 0) {
            ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
        }
        return ipAddress;
    }


}

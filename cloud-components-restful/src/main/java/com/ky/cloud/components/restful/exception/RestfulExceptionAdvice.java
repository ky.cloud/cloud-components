package com.ky.cloud.components.restful.exception;


import com.ky.cloud.components.restful.HttpResult;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;


/**
 * restful异常处理
 *
 * @author chenHui
 * @version 1.0.0
 */
@Slf4j
@RestControllerAdvice
public class RestfulExceptionAdvice {

    /**
     * 400 - Bad Request
     *
     * @since 1.0.0
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public HttpResult handleHttpMessageNotReadableException(HttpServletRequest request, Exception e) {
        log.error("请求[{}],参数解析失败", request, e);
        return HttpResult.failure("Bad Request");
    }

    /**
     * 400 - Bad Request
     *
     * @since 1.0.0
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public HttpResult methodArgumentTypeMismatchException(Exception e) {
        log.error("请求异常",e);
        return HttpResult.failure("Bad Request");
    }


    /**
     * 405 - Method Not Allowed
     *
     * @since 1.0.0
     */
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public HttpResult handleHttpRequestMethodNotSupportedException(Exception e) {
        log.error("请求方式异常",e);
        return HttpResult.failure("请求方式异常");
    }


    /**
     * 处理 form data方式调用接口校验失败抛出的异常
     *
     * @since 1.0.0
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(BindException.class)
    public HttpResult bindExceptionHandler(BindException e) {
        List<FieldError> fieldErrors = e.getBindingResult().getFieldErrors();
        //只返回第一个信息
        ObjectError error = fieldErrors.get(0);
        //返回自定义信息格式
        return HttpResult.failure(error.getDefaultMessage());
    }

    /**
     * 处理请求参数@RequestBody校验异常
     *
     * @since 1.0.0
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public HttpResult handleConstraintViolationException(MethodArgumentNotValidException e) {
        //获取所有错误异常
        List<ObjectError> allErrors = e.getBindingResult().getAllErrors();
        //只返回第一个信息
        ObjectError error = allErrors.get(0);
        return HttpResult.failure(error.getDefaultMessage());
    }

    /**
     * 请求参数@RequestParam校验异常
     *
     * @since 1.0.0
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody
    public HttpResult handleConstraintViolationException(ConstraintViolationException e) {
        // 获取所有错误信息
        HashSet<ConstraintViolation<?>> set = (HashSet<ConstraintViolation<?>>) e.getConstraintViolations();
        Iterator<ConstraintViolation<?>> iterator = set.iterator();
        if (iterator.hasNext()) {
            ConstraintViolation<?> next = iterator.next();
            // 只取一个异常信息返回
            String msg = next.getMessageTemplate();
            return HttpResult.failure(msg);
        }
        return HttpResult.failure("参数异常");
    }

    /**
     * 业务异常
     *
     * @since 1.0.0
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(BusinessException.class)
    public HttpResult handleBusinessException(Exception e) {
        log.error("服务运行异常,[{}]",e.getMessage());
        return HttpResult.failure(e.getMessage());
    }

    /**
     * 500 - Internal Server Error
     *
     * @since 1.0.0
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public HttpResult handleException(Exception e) {
        log.error("服务运行异常,[{}]", e.getMessage(),e);
        return HttpResult.failure("系统异常");
    }
}

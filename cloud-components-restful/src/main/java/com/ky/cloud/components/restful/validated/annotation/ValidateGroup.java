package com.ky.cloud.components.restful.validated.annotation;


import jakarta.validation.groups.Default;

/**
 * 校验分组注释
 *
 * @author chenHui
 * @version 1.0.0
 */
public interface ValidateGroup {

    /**
     * 保存校验
     */
    interface InsertCheck extends Default {}

    /**
     * 修改校验
     */
    interface UpdateCheck extends Default{}

    /**
     * 删除校验
     */
    interface DeleteCheck extends Default{}

    /**
     * 查询校验
     */
    interface QueryCheck extends Default{}

}

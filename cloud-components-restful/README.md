# cloud-components-restful
针对restful常用开发功能整合，例如有restful统一出参,异常统一拦截处理，restful出入参日志打印，restful入参校验等功能

## version:1.0.0 
> + 异常统一拦截处理
> + 出入参日志打印，日志添加tranId并返回给前端
> + 通过Validated进行入参校验
> + 支持获取登录用户放置到上下文
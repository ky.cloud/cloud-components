package com.ky.cloud.components.context.constant;

/**
 * rpc常量
 *
 * @author chenHui
 * @version 1.0.0
 */
public class TraceConstant {
    /**
     *  traceId
     */
    public static final String MDC_TRACE_ID = "traceId";

    /**
     *  spanId
     */
    public static final String MDC_SPAN_ID = "spanId";

    /**
     *  请求系统
     */
    public static final String REQ_APP = "reqApp";

    /**
     *  操作人
     */
    public static final String OPERATOR = "operator";


}

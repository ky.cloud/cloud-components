package com.ky.cloud.components.context;

import jakarta.annotation.Nonnull;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Spring上下文
 *
 * @author chenHui
 * @version 1.0.0
 */
public class SpringContext implements ApplicationContextAware {
    private static ApplicationContext applicationContext = null;

    public static ApplicationContext getApplicationContext(){
        return applicationContext;
    }

    @Override
    public void setApplicationContext(@Nonnull ApplicationContext applicationContext) throws BeansException {
        if (SpringContext.applicationContext == null){
            SpringContext.applicationContext = applicationContext;
        }
    }

    /**
     * 获取SpringBean
     *
     * @author chenHui
     * @since  1.0.0
     */
    public static <T>T getBean(Class<T> tClass){
        String beanName = tClass.getName();
        if (!applicationContext.containsBean(beanName)){
            throw new BeanCreationException(beanName+"不存在");
        }
        return getApplicationContext().getBean(tClass);
    }
}

package com.ky.cloud.components.context;

import com.alibaba.ttl.TransmittableThreadLocal;
import com.ky.cloud.components.context.constant.TraceConstant;
import org.slf4j.MDC;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

/**
 * 系统上下文
 *
 * @author chenHui
 * @version 1.0.0
 */
public class SystemContext {

    private static final String DEFAULT_SPAN = "0";
    private static ThreadLocal<SystemContext> threadLocal = new TransmittableThreadLocal<>();
    /**
     * 操作用户
     */
    private String traceId;
    /**
     * 操作用户
     */
    private String spanId;
    /**
     * 操作用户
     */
    private String operator;

    private Map<String,Object> contextParam = new HashMap<>();

    public static ThreadLocal<SystemContext> getThreadLocal() {
        return threadLocal;
    }

    public static void setThreadLocal(ThreadLocal<SystemContext> threadLocal) {
        SystemContext.threadLocal = threadLocal;
    }

    public static SystemContext getContext() {
        SystemContext context = threadLocal.get();
        if (Objects.isNull(context)) {
            context = initContext();
        }
        return context;
    }

    private static void setContext(SystemContext context) {
        threadLocal.set(context);
    }

    public static void nextSpan() {
        SystemContext context = getContext();
        String spanId = context.getSpanId();
        if (Objects.isNull(spanId)) {
            context.setSpanId(DEFAULT_SPAN);
            return;
        }
        int newSpanId = Integer.parseInt(spanId) + 1;
        context.setSpanId(String.valueOf(newSpanId));
    }

    public static void nextSpan(String spanId) {
        SystemContext context = getContext();
        if (Objects.isNull(spanId)) {
            context.setSpanId(DEFAULT_SPAN);
            return;
        }
        int newSpanId = Integer.parseInt(spanId) + 1;
        context.setSpanId(String.valueOf(newSpanId));
    }

    private static SystemContext initContext() {
        SystemContext context = new SystemContext();
        String traceId = UUID.randomUUID().toString().trim().replace("-", "");
        context.setTraceId(traceId);
        context.setSpanId(DEFAULT_SPAN);
        context.setOperator("system");
        setContext(context);
        return context;
    }


    public static SystemContext initContext(String traceId, String spanId) {
        SystemContext context = getContext();
        if (StringUtils.hasText(traceId)){
            context.setTraceId(traceId);
        }
        if (StringUtils.hasText(spanId)) {
            context.setSpanId(spanId);
        }
        return context;
    }

    public static void addOperator(String operator){
        SystemContext context = getContext();
        context.setOperator(operator);
        setContext(context);
    }
    /**
     * 添加context指定参数
     *
     * @param map key 参数名称 value 参数
     * @since 1.0.0
     */
    public static void addContextParam(Map<String,Object> map) {
        SystemContext context = getContext();
        Map<String, Object> contextParam = context.getContextParam();
        contextParam.putAll(map);
        setContext(context);
    }

    /**
     * 添加context指定参数
     *
     * @param key 参数名称 value 参数
     * @since 1.0.0
     */
    public static void addContextParam(String key,Object value) {
        SystemContext context = getContext();
        Map<String, Object> contextParam = context.getContextParam();
        contextParam.put(key,value);
        setContext(context);
    }

    /**
     * 获取context设定参数
     *
     * @param key 设定参数key
     * @return Object 参数
     * @since 1.0.0
     */
    public static Object getContextParam(String key){
        SystemContext context = getContext();
        return context.getContextParam().get(key);
    }


    public static void remove(){
        threadLocal.remove();
    }

    public static void initTraceMdc(){
        SystemContext context = getContext();
        MDC.put(TraceConstant.MDC_TRACE_ID, context.getTraceId());
        MDC.put(TraceConstant.MDC_SPAN_ID, context.getSpanId());
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getSpanId() {
        return spanId;
    }

    public void setSpanId(String spanId) {
        this.spanId = spanId;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Map<String, Object> getContextParam() {
        return contextParam;
    }

    public Object getContextParamValue(String key) {
        return contextParam.get(key);
    }

    public void setContextParam(Map<String, Object> contextParam) {
        this.contextParam = contextParam;
    }
}

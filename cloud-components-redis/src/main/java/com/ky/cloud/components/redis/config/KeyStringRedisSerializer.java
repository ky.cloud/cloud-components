package com.ky.cloud.components.redis.config;

import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

import java.nio.charset.Charset;

/**
 * redis主键序列
 *
 * @author chenHui
 * @version 1.0.0
 */
public class KeyStringRedisSerializer implements RedisSerializer<String> {
    private String application;


    public KeyStringRedisSerializer(String application) {
        this.application = application;
    }

    @Override
    public byte[] serialize(String s) throws SerializationException {
        return (s == null ?null:(application.replace("-",".")+"."+s).getBytes(Charset.defaultCharset()));
    }

    @Override
    public String deserialize(byte[] bytes) throws SerializationException {
        if (bytes==null){
            return null;
        } else {
            return new String(bytes,Charset.defaultCharset()).replaceFirst(application.replace("-","."), "");
        }
    }
}

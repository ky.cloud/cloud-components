package com.ky.cloud.components.redis.util;


import com.alibaba.fastjson2.JSON;
import io.lettuce.core.RedisException;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * redis工具类
 *
 * @author chenHui
 * @version 1.0.0
 */
@Slf4j
@Component
public class RedisUtils {
    @Resource(name = "redisTemplate")
    private RedisTemplate<String, Object> redisTemplate;
    @Resource(name = "stringRedisTemplate")
    private StringRedisTemplate stringRedisTemplate;


    /**
     * 判断key缓存中是否存在
     *
     * @param key redisKey
     * @return boolean
     * @since 1.0.0
     */
    public boolean hasKey(String key) {
        try {
            return Boolean.TRUE.equals(redisTemplate.hasKey(key));
        } catch (Exception e) {
            throw new RedisException(e);
        }
    }

    /**
     * 缓存获取
     *
     * @param key redisKey
     * @return Object 缓存数据
     * @since 1.0.0
     */
    public Object getObject(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    /**
     * 缓存获取
     *
     * @param key redisKey
     * @return Object 缓存数据
     * @since 1.0.0
     */
    public String getString(String key) {
        return stringRedisTemplate.opsForValue().get(key);
    }

    /**
     * 缓存获取
     *
     * @param key redisKey
     * @return Map<?, ?> 缓存数据
     * @since 1.0.0
     */
    public Map getMap(String key) {
        return redisTemplate.opsForHash().entries(key);
    }

    /**
     * 缓存存放
     *
     * @param key   redisKey
     * @param value 存放数据
     * @return boolean
     * @since 1.0.0
     */
    public boolean set(String key, Object value) {
        try {
            redisTemplate.opsForValue().set(key, value);
            return true;
        } catch (Exception e) {
            log.error("redis set异常,key[{}],value[{}]", key, JSON.toJSONString(value), e);
            return false;
        }
    }

    /**
     * 缓存存放
     *
     * @param key   redisKey
     * @param value 存放数据
     * @param time  过期时间(秒)
     * @return boolean
     * @since 1.0.0
     */
    public boolean set(String key, Object value, long time) {
        try {
            if (time > 0) {
                redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
            } else {
                set(key, value);
            }
            return true;
        } catch (Exception e) {
            log.error("redis set异常,key[{}],value[{}],time[{}]", key, JSON.toJSONString(value), time, e);
            return false;
        }
    }


    /**
     * 缓存存放
     *
     * @param key   redisKey
     * @param value 存放数据
     * @return boolean
     * @since 1.0.0
     */
    public boolean set(String key, Map<?, ?> value) {
        redisTemplate.opsForHash().putAll(key, value);
        return true;
    }


    /**
     * 缓存删除
     *
     * @param key redisKey
     * @return boolean
     * @since 1.0.0
     */
    public boolean delete(String key) {
        return Boolean.TRUE.equals(redisTemplate.delete(key));
    }

    /**
     * 新增
     *
     * @param key   键
     * @param delta 递增因子
     * @return 值
     */
    public Long inc(String key, long delta) {
        if (delta < 0L) {
            throw new RedisException("递增因子必须大于0");
        } else {
            return this.redisTemplate.opsForValue().increment(key, delta);
        }
    }

    /**
     * 递减
     *
     * @param key   键
     * @param delta 要减少几(小于0)
     */
    public Long decr(String key, long delta) {
        if (delta < 0) {
            throw new RedisException("递减因子必须大于0");
        } else {
            return this.redisTemplate.opsForValue().decrement(key, -delta);
        }
    }


    /**
     * 拼接字符
     *
     * @param key       字符
     * @param namespace 字符
     * @return 拼接后的值
     */
    public static String cacheKey(String key, String namespace) {
        return namespace + "." + key;
    }
}

#set(tableComment = table.getComment())
#set(entityClassName = table.buildEntityClassName())
#set(entityVarName = firstCharToLowerCase(entityClassName))
#set(serviceVarName = firstCharToLowerCase(table.buildServiceClassName()))
package #(packageConfig.controllerPackage);

import com.ky.cloud.components.restful.HttpResult;
import com.ky.cloud.components.restful.model.PageQuery;
import com.ky.cloud.components.restful.validated.annotation.ValidateGroup;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import jakarta.annotation.Resource;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import #(packageConfig.entityPackage).#(entityClassName);
import #(packageConfig.servicePackage).#(table.buildServiceClassName());

#if(withSwagger && swaggerVersion.getName() == "DOC")
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
#end
import java.util.List;

/**
 * #(tableComment) 控制层。
 *
 * @author #(javadocConfig.getAuthor())
 * @since #(javadocConfig.getSince())
 */
#if(controllerConfig.restStyle)
@RestController
#else
@Controller
#end
#if(withSwagger && swaggerVersion.getName() == "FOX")
@Api("#(tableComment)接口")
#end
#if(withSwagger && swaggerVersion.getName() == "DOC")
@Tag(name = "#(tableComment)接口")
#end
@RequestMapping("/#(firstCharToLowerCase(entityClassName))")
public class #(table.buildControllerClassName()) #if(controllerConfig.superClass)extends #(controllerConfig.buildSuperClassName()) #end {

    @Resource
    private #(table.buildServiceClassName()) #(serviceVarName);

    /**
     * 新增数据
     *
     * @param #(entityVarName)ReqVO 新增数据请求对象
     * @return HttpResult
     * @since #(javadocConfig.getSince())
     */
    @PostMapping()
    #if(withSwagger && swaggerVersion.getName() == "DOC")
    @Operation(description="新增数据")
    #end
    public HttpResult add(@RequestBody @Validated(ValidateGroup.InsertCheck.class)  #if(withSwagger && swaggerVersion.getName() == "DOC")@Parameter(description="#(tableComment)")#end #(entityClassName)ReqVO #(entityVarName)ReqVO) {
        QueryWrapper queryWrapper = QueryWrapper.create();
        // TODO 设置查询条件
        if (#(serviceVarName).exists(queryWrapper)) {
            return HttpResult.failure("数据已经存在");
        }
        #(entityClassName) #(entityVarName) = new #(entityClassName)();
        BeanUtils.copyProperties(#(entityVarName)ReqVO, #(entityVarName));

        if(!#(serviceVarName).save(#(entityVarName))){
            return HttpResult.failure("新增任务失败，IT处理");
        }
        return HttpResult.success();
    }

    /**
     * 删除数据
     *
     * @param #(entityVarName)ReqVO #(tableComment)ReqVO
     * @return HttpResult
     * @since #(javadocConfig.getSince())
     */
    @DeleteMapping()
    #if(withSwagger && swaggerVersion.getName() == "DOC")
    @Operation(description="删除数据")
    #end
    public HttpResult remove(@RequestBody @Validated(ValidateGroup.DeleteCheck.class) #(entityClassName)ReqVO #(entityVarName)ReqVO) {
        if(#(serviceVarName).removeById(#(entityVarName)ReqVO.get#(entityClassName)Id)){
            return HttpResult.success();
        }
        return HttpResult.failure("删除失败");
    }

    /**
     * 更新数据
     *
     * @param #(entityVarName)ReqVO 更新数据请求对象
     * @return HttpResult
     * @since #(javadocConfig.getSince())
     */
    @PutMapping()
    #if(withSwagger && swaggerVersion.getName() == "DOC")
    @Operation(description="更新数据")
    #end
    public HttpResult edit(@RequestBody @Validated(ValidateGroup.UpdateCheck.class) #(entityClassName)ReqVO #(entityVarName)ReqVO) {
        #(entityClassName) #(entityVarName) = new #(entityClassName)();
        BeanUtils.copyProperties(#(entityVarName)ReqVO, #(entityVarName));

        if (!#(serviceVarName).updateById(#(entityVarName))){
            return HttpResult.failure("更新任务失败，IT处理");
        }
        return HttpResult.success();
    }


    /**
     * 分页查询#(tableComment)。

     * @param #(entityVarName)ReqVO 分页查询数据请求对象
     * @param pageQuery 分页对象
     * @return HttpResult 分页对象
     * @since #(javadocConfig.getSince())
     */
    @GetMapping()
    #if(withSwagger && swaggerVersion.getName() == "DOC")
    @Operation(description="分页查询#(tableComment)")
    #end
    public HttpResult pageQuery(#if(withSwagger && swaggerVersion.getName() == "DOC")@Parameter(description="分页信息")#end #(entityClassName)ReqVO #(entityVarName)ReqVO,PageQuery pageQuery) {
        #(entityClassName) #(entityVarName) = new #(entityClassName)();
        BeanUtils.copyProperties(#(entityVarName)ReqVO, #(entityVarName));
        QueryWrapper queryWrapper = QueryWrapper.create(#(entityVarName));
        Page<#(entityClassName)> page = #(serviceVarName).page(Page.of(pageQuery.getPageIndex(), pageQuery.getPageSize()), queryWrapper);

        List<#(entityClassName)> dataList = page.getRecords();
        List<#(entityClassName)ResVO> resVOList = BeanCopyUtil.copyListProperties(dataList, #(entityClassName)ResVO::new);

        Page<#(entityClassName)ResVO> resVOPage = new Page<>();
        BeanUtils.copyProperties(page, resVOPage);
        resVOPage.setRecords(resVOList);
        return HttpResult.success(resVOPage);
    }

}

package com.ky.cloud.components.mybatis.model;


import com.mybatisflex.annotation.Column;

import java.util.Date;

/**
 * 公共实体类
 *
 * @author chenHui
 * @version 1.0.0
 */
public class BaseEntity {
    /**
     * 创建时间
     */
    protected Date createTime;

    /**
     * 创建人
     */
    protected String createBy;

    /**
     * 更新时间
     */
    protected Date updateTime;

    /**
     * 更新人
     */
    protected String updateBy;

    /**
     * 删除标志
     */
    @Column(isLogicDelete = true)
    protected Boolean isDelete;

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Boolean getDelete() {
        return isDelete;
    }

    public void setDelete(Boolean delete) {
        isDelete = delete;
    }
}

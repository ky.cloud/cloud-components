package com.ky.cloud.components.mybatis.config;

import com.ky.cloud.components.context.SystemContext;
import com.ky.cloud.components.mybatis.model.BaseEntity;
import com.mybatisflex.annotation.InsertListener;

import java.util.Date;

/**
 * 实体类insert监听类
 *
 * @author chenHui
 * @version 1.0.0
 */
public class EntityInsertListener implements InsertListener {

    @Override
    public void onInsert(Object o) {

        BaseEntity baseEntity = (BaseEntity) o;
        baseEntity.setCreateTime(new Date());
        baseEntity.setUpdateTime(new Date());

        String operator = SystemContext.getContext().getOperator();
        baseEntity.setCreateBy(operator);
        baseEntity.setUpdateBy(operator);
    }
}

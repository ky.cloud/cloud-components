package com.ky.cloud.components.mybatis.config;

import com.ky.cloud.components.context.SystemContext;
import com.ky.cloud.components.mybatis.model.BaseEntity;
import com.mybatisflex.annotation.UpdateListener;

import java.util.Date;
/**
 * 实体类update监听类
 *
 * @author chenHui
 * @version 1.0.0
 */
public class EntityUpdateListener implements UpdateListener {


    @Override
    public void onUpdate(Object o) {
        BaseEntity baseEntity = (BaseEntity) o;
        baseEntity.setUpdateTime(new Date());
        baseEntity.setUpdateBy(SystemContext.getContext().getOperator());
    }
}

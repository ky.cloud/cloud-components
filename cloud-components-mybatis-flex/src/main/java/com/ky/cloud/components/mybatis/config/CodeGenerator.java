package com.ky.cloud.components.mybatis.config;

import com.ky.cloud.components.mybatis.model.BaseEntity;
import com.ky.cloud.components.mybatis.model.CodeGeneratorParam;
import com.mybatisflex.codegen.Generator;
import com.mybatisflex.codegen.config.*;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.util.StringUtils;

/**
 * 代码生成类
 *
 * @author chenHui
 * @version 1.0.0
 */
public class CodeGenerator {

    public static void generator(CodeGeneratorParam codeGeneratorParam) {

        String url= codeGeneratorParam.getUrl();
        String username= codeGeneratorParam.getUsername();
        String password= codeGeneratorParam.getPassword();

        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);

        GlobalConfig globalConfig = new GlobalConfig();
        fillGlobalConfig(globalConfig);
        fillJavadocConfig(globalConfig,codeGeneratorParam);
        fillPageConfig(globalConfig,codeGeneratorParam);
        fillStrategyConfig(globalConfig,codeGeneratorParam);
        fillTemplateConfig(globalConfig);

        Generator generator = new Generator(dataSource, globalConfig);
        generator.generate();
    }

    /**
     * 设置全局配置
     *
     * @since  1.0.0
     */
    protected static void fillGlobalConfig(GlobalConfig globalConfig){
        globalConfig.enableEntity().setWithLombok(true).setSuperClass(BaseEntity.class).setOverwriteEnable(true);
        globalConfig.enableMapper();
        globalConfig.enableMapperXml();
        globalConfig.enableService();
        globalConfig.enableServiceImpl();
        globalConfig.enableController();
    }

    /**
     * 设置注释配置
     *
     * @since  1.0.0
     */
    protected static void fillJavadocConfig(GlobalConfig globalConfig,CodeGeneratorParam codeGeneratorParam){
        JavadocConfig javadocConfig = globalConfig.getJavadocConfig();
        if (StringUtils.hasText(codeGeneratorParam.getAuthor())){
            javadocConfig.setAuthor(codeGeneratorParam.getAuthor());
        }
        if (StringUtils.hasText(codeGeneratorParam.getSince())) {
            javadocConfig.setSince(codeGeneratorParam.getSince());
        }
    }

    /**
     * 设置包配置
     *
     * @since  1.0.0
     */
    protected static void fillPageConfig(GlobalConfig globalConfig,CodeGeneratorParam codeGeneratorParam){
        PackageConfig packageConfig = globalConfig.getPackageConfig();
        String appName = codeGeneratorParam.getAppName();
        if (StringUtils.hasText(appName)){
            packageConfig.setSourceDir(System.getProperty("user.dir") +"/"+ appName + "/src/main/java");
            packageConfig.setMapperXmlPath(System.getProperty("user.dir") +"/"+ appName + "/src/main/resources/mapper");

        }
        packageConfig.setBasePackage(codeGeneratorParam.getBasePackage());
    }

    /**
     * 设置包配置
     *
     * @since  1.0.0
     */
    protected static void fillTemplateConfig(GlobalConfig globalConfig){
        globalConfig.getTemplateConfig()
                .setController("/templates/controller.tpl");
    }

    /**
     * 设置策略配置
     *
     * @since  1.0.0
     */
    protected static void fillStrategyConfig(GlobalConfig globalConfig,CodeGeneratorParam codeGeneratorParam){
        StrategyConfig strategyConfig = globalConfig.getStrategyConfig();
        strategyConfig.setIgnoreColumns("create_time","create_by","update_time","update_by","is_Delete");
        strategyConfig.setGenerateTables(codeGeneratorParam.getTables());
        if (StringUtils.hasText(codeGeneratorParam.getTablePrefix())){
            strategyConfig.setTablePrefix(codeGeneratorParam.getTablePrefix());
        }
        TableConfig tableConfig = new TableConfig();
        tableConfig.setInsertListenerClass(EntityInsertListener.class);
        tableConfig.setUpdateListenerClass(EntityUpdateListener.class);
        strategyConfig.setTableConfig(tableConfig);
    }
}

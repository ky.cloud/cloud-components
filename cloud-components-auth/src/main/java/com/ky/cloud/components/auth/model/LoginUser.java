package com.ky.cloud.components.auth.model;

import com.alibaba.fastjson2.JSONObject;

import java.io.Serializable;

/**
 * @author chenHui
 * @version 1.0.0
 */
public class LoginUser implements Serializable {
    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 用户号
     */
    private String userNo;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 用户名称
     */
    private String comCode = "86";

    /**
     * 用户名称
     */
    private String codeName = "总公司";

    /**
     * 备注
     */
    private JSONObject remark;
    /**
     * 登录ip
     */
    private String loginIp;

    public void setComCode(String comCode) {
        this.comCode = comCode;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public String getComCode() {
        return comCode;
    }

    public String getCodeName() {
        return codeName;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public JSONObject getRemark() {
        return remark;
    }

    public void setRemark(JSONObject remark) {
        this.remark = remark;
    }


    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }
}

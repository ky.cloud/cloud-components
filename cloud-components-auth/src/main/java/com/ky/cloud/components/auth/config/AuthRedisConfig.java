package com.ky.cloud.components.auth.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.time.Duration;

/**
 * 用户权限redis配置
 *
 * @author chenHui
 * @version 1.0.0
 */
@Data
@Component
@ConfigurationProperties(prefix = "auth.redis")
public class AuthRedisConfig {

    private String host;

    private Integer port;

    private String password;

    private Integer database;

    private Integer timeout;

    private Integer maxActive;

    private Integer minIdle;

    private Integer maxWait;



    @Bean(name = "authCache")
    @SuppressWarnings(value = { "unchecked", "rawtypes" })
    public StringRedisTemplate getAuthCache() {
        RedisStandaloneConfiguration configuration = new RedisStandaloneConfiguration();
        configuration.setHostName(host);
        configuration.setPort(port);
        if (StringUtils.hasText(password)) {
            configuration.setPassword(password);
        }
        if (database != 0) {
            configuration.setDatabase(database);
        }

        LettuceClientConfiguration clientConfig = LettuceClientConfiguration.builder()
                .commandTimeout(Duration.ofMillis(timeout))
                .build();

        LettuceConnectionFactory factory = new LettuceConnectionFactory(configuration,clientConfig);
        factory.afterPropertiesSet();

        // 解决redis客户端查看 value乱码问题
        FastJson2JsonRedisSerializer serializer = new FastJson2JsonRedisSerializer(Object.class);
        StringRedisTemplate redisTemplate = new StringRedisTemplate(factory);
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(serializer);

        return redisTemplate;
    }
}

package com.ky.cloud.components.auth;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.ky.cloud.components.auth.config.AuthConfig;
import com.ky.cloud.components.auth.model.LoginUser;
import jakarta.annotation.Resource;
import com.ky.cloud.components.context.SystemContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * 用户权限工具类
 *
 * @author chenHui
 * @version 1.0.0
 */
@Slf4j
@Component
public class AuthUtils {
    @Resource(name = "authCache")
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private AuthConfig authConfig;

    private static final String REDIS_PREFIX="auth.token.";
    /**
     * 获取用户token
     *
     * @param loginUser LoginUser
     * @return String token
     * @since 1.0.0
     */
    public String getUserToken(LoginUser loginUser){
        String token = UUID.randomUUID().toString();
        stringRedisTemplate.opsForValue().set(REDIS_PREFIX+ token, JSON.toJSONString(loginUser),60, TimeUnit.MINUTES);
        return token;
    }

    /**
     * 删除缓存中的token
     *
     * @param token 用户token
     * @return boolean
     * @since 1.0.0
     */
    public boolean deleteToken(String token){
        return Boolean.TRUE.equals(stringRedisTemplate.opsForValue().getOperations().delete(REDIS_PREFIX + token));
    }


    /**
     * 校验token
     *
     * @param token 用户token
     * @param reqIp 请求ip
     * @return boolean
     * @since 1.0.0
     */
    public boolean checkToken(String token,String reqIp){
        log.info("鉴权校验入参 token:{},reqIp:{}",token,reqIp);
        String loginUserString = stringRedisTemplate.opsForValue().get(REDIS_PREFIX + token);
        if (!StringUtils.hasText(loginUserString)){
            log.info("鉴权校验失败 token不存在");
            return false;
        }
        LoginUser loginUser = JSONObject.parseObject(loginUserString, LoginUser.class);
        return loginUser.getLoginIp().equals(reqIp);
    }

    /**
     * 获取登录用户
     *
     * @param token 用户token
     * @return LoginUser
     * @since 1.0.0
     */
    public LoginUser getLoginUser(String token){
        String userString = stringRedisTemplate.opsForValue().get(REDIS_PREFIX + token);
        if (!StringUtils.hasText(userString)){
            return null;
        }
        return JSON.parseObject(userString,LoginUser.class);
    }

    /**
     * 登录用户添加到上下文
     *
     * @param loginUser LoginUser
     * @since 1.0.0
     */
    public void addUserToContext(LoginUser loginUser){
        Map<String, Object> map = new HashMap<>();
        map.put("user", loginUser);
        SystemContext.addOperator(loginUser.getUserNo());
        SystemContext.addContextParam(map);
    }

    /**
     * 获取操作用户
     *
     * @return String
     * @since 1.0.0
     */
    public String getOperator(){
        return SystemContext.getContext().getOperator();
    }


    /**
     * 获取操作用户信息
     *
     * @return String 登录用户对象
     * @since 1.0.0
     */
    public LoginUser getUser(){
        Object loginUser = SystemContext.getContextParam("user");
        if (loginUser == null){
            return null;
        }
        return (LoginUser) loginUser;
    }


    /**
     * 是否需要鉴权
     *
     * @param  path 请求url
     * @return boolean true 需要鉴权,false 不需要鉴权
     * @since 1.0.0
     */
    public boolean isNeedAuth(String path){
        path = path.replace("//", "/");
        List<String> whiteList = authConfig.getWhiteList();
        for (String whiteUrl : whiteList) {
            if (path.startsWith(whiteUrl)) {
                return false;
            }
        }
        return true;
    }


}

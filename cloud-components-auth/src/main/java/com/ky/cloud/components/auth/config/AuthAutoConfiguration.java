package com.ky.cloud.components.auth.config;

import com.ky.cloud.components.auth.AuthUtils;
import jakarta.annotation.Resource;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author chenhui_sinosoft
 * @version 1.0.0
 */
@AutoConfiguration
@ComponentScan(basePackages = {"com.ky.cloud.components.auth"})
public class AuthAutoConfiguration {
    @Resource
    private AuthConfig authConfig;
    @Resource
    private AuthRedisConfig authRedisConfig;

    @Resource
    private AuthUtils authUtils;
    @Bean(name = "authConfig")
    public AuthConfig getAuthConfig(){
        return authConfig;
    }

    @Bean(name = "authRedisConfig")
    public AuthRedisConfig getAuthRedisConfig(){
        return authRedisConfig;
    }

    @Bean(name = "authUtils")
    public AuthUtils getAuthUtils(){
        return authUtils;
    }
}

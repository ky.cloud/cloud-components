# cloud-components
> 组件服务
> - cloud-component-auth 用户权限组件
> - cloud-component-batch-powerjob  批处理powerjob组件
> - cloud-component-config-nacos 配置中心nacos组件
> - cloud-component-context 上下文组件
> - cloud-component-dubbo 远程调用dubbo组件
> - cloud-component-file 文件存储组件
> - cloud-component-mybatis-flex mybatis-flex组件
> - cloud-component-mybatis-plus mybatis-plus组件
> - cloud-component-redis 缓存redis组件
> - cloud-component-restful restful组件
> - cloud-component-restful 消息队列rocketmq组件








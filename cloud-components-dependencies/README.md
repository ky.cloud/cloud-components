# cloud-components-dependencies
微服务组件依赖

## version:1.0.0
支持组件
> + cloud-components-auth （权限组件）
> + cloud-components-context （系统上下文组件）
> + cloud-components-dubbo （dubbo组件）
> + cloud-components-file （文件云存储组件）
> + cloud-components-nacos （配置中心nacos组件）
> + cloud-components-redis （缓存redis组件）
> + cloud-components-restful （restful组件）
> + cloud-components-rocketmq （消息队列rocket组件）
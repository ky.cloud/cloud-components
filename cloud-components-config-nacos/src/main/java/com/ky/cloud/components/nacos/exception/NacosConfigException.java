package com.ky.cloud.components.nacos.exception;

/**
 * nacos配置异常
 *
 * @author chenHui
 * @version 1.0。0
 */
public class NacosConfigException extends RuntimeException {

    public NacosConfigException() {
        super();
    }

    public NacosConfigException(String message) {
        super(message);
    }

    public NacosConfigException(String dataId, String errorDesc) {
        super("nacos配置dataId:[" + dataId + "]" + errorDesc);
    }


}

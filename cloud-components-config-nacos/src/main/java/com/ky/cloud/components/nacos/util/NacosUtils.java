package com.ky.cloud.components.nacos.util;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.PropertyKeyConst;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.exception.NacosException;
import com.ky.cloud.components.nacos.config.NacosConfig;
import com.ky.cloud.components.nacos.exception.NacosConfigException;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Properties;

/**
 * nacos工具类
 *
 * @author chenHui
 * @version 1.0.0
 */
public class NacosUtils {
    private final ConfigService configService;

    private final NacosConfig config;

    public NacosUtils(NacosConfig nacosConfig) throws NacosException {
        this.config = nacosConfig;
        Properties properties = new Properties();
        properties.put(PropertyKeyConst.SERVER_ADDR, config.getServerAddr());
        properties.put(PropertyKeyConst.USERNAME, config.getUsername());
        properties.put(PropertyKeyConst.PASSWORD, config.getPassword());
        properties.put(PropertyKeyConst.NAMESPACE, config.getNamespace());
        this.configService = NacosFactory.createConfigService(properties);
    }

    /**
     * 获取配置中心信息
     *
     * @param dataId 配置中心路径
     * @return 配置中心数据
     * @throws NacosConfigException nacos配置异常
     * @since 1.0.0
     */
    public String getNacosData(String dataId) {
        if (!StringUtils.hasText(dataId)) {
            throw new NacosConfigException("dataId不能为空");
        }
        try {
            String nacosData = configService.getConfig(dataId, config.getGroup(), 3000);
            if (!StringUtils.hasText(nacosData)) {
                throw new NacosConfigException(dataId, "配置为空");
            }
            return nacosData;
        } catch (NacosException e) {
            throw new NacosConfigException("获取nacos配置异常"+e);
        }
    }


    /**
     * 获取配置中心信息
     *
     * @param dataId 配置中心配置路径
     * @param clazz  转换的dto类型
     * @return 转换的dto集合
     * @throws NacosConfigException nacos配置异常
     * @since 1.0.0
     */
    public <T> T getNacosDataToDTO(String dataId, Class<T> clazz) {
        List<T> nacosDataToDTOList = this.getNacosDataToDTOList(dataId, clazz);
        if (nacosDataToDTOList.size() > 1) {
            throw new NacosConfigException(dataId, "配置大于1条");
        }
        return nacosDataToDTOList.get(0);
    }

    /**
     * 获取配置中心信息
     *
     * @param dataId 配置中心配置路径
     * @param clazz  转换的dto类型
     * @return 转换的dto集合
     * @throws NacosConfigException nacos配置异常
     * @since 1.0.0
     */
    public <T> List<T> getNacosDataToDTOList(String dataId, Class<T> clazz) {
        String nacosData = this.getNacosData(dataId);
        return JSON.parseArray(nacosData, clazz);
    }


    /**
     * 获取配置中心信息
     *
     * @param dataId 配置中心路径
     * @return JSONObject
     * @throws NacosConfigException nacos配置异常
     * @since 1.0.0
     */
    public JSONObject getConfigInfoMap(String dataId) {
        String jsonString = this.getNacosData(dataId);
        return JSON.parseObject(jsonString);
    }
}

package com.ky.cloud.components.nacos.config;

import com.alibaba.nacos.api.exception.NacosException;
import com.ky.cloud.components.nacos.util.NacosUtils;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author chenhui_sinosoft
 * @version 1.0.0
 */
@AutoConfiguration
@ComponentScan(basePackages = {"com.ky.cloud.components.nacos"})
public class NacosAutoConfiguration {
    private NacosConfig nacosConfig;

    @Bean(name = "nacosConfig")
    public NacosConfig getNacosConfig(){
        this.nacosConfig = new NacosConfig();
        return nacosConfig;
    }

    @Bean(name = "nacosUtils")
    public NacosUtils getNacosUtils() throws NacosException {
        return new NacosUtils(nacosConfig);
    }
}

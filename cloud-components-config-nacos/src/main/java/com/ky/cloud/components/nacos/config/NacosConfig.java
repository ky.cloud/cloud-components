package com.ky.cloud.components.nacos.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * nacos配置
 *
 * @author chenHui
 * @version 1.0.0
 */
@Data
@ConfigurationProperties(prefix = "spring.cloud.nacos.config")
public class NacosConfig {
    /**
     * nacos配置中心地址
     */
    private String serverAddr;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 工作空间
     */
    private String namespace;
    /**
     * 分组
     */
    private String group;
}

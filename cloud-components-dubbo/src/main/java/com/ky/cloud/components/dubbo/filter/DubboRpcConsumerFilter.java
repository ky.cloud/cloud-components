package com.ky.cloud.components.dubbo.filter;

import com.alibaba.fastjson2.JSON;
import com.ky.cloud.components.context.SystemContext;
import com.ky.cloud.components.context.constant.TraceConstant;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.*;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.io.InputStream;
import java.util.Arrays;

/**
 * DubboRpc客户端过滤器
 *
 * @author chenHui
 * @version 1.0.0
 */
@Slf4j
@Activate(group = CommonConstants.CONSUMER, order = Integer.MIN_VALUE)
public class DubboRpcConsumerFilter implements Filter {
    @Value("${spring.application.name}")
    private String reqApp;

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        long start = System.currentTimeMillis();
        SystemContext context = SystemContext.getContext();

        RpcContext.getClientAttachment()
                .setAttachment(TraceConstant.MDC_TRACE_ID, context.getTraceId())
                .setAttachment(TraceConstant.MDC_SPAN_ID, context.getSpanId())
                .setAttachment(TraceConstant.REQ_APP, reqApp)
                .setAttachment(TraceConstant.OPERATOR, context.getOperator());
        Result result = invoker.invoke(invocation);
        long end = System.currentTimeMillis();
        printLog(invocation, result, end - start);
        return result;
    }


    /***
     *  打印结果
     * @author winfun
     * @param invocation invocation
     * @param result result
     **/
    private void printLog(Invocation invocation, Result result, long spendTime) {
        String interfaceClass = invocation.getInvoker().getInterface().getName();
        String methodName = invocation.getMethodName();
        Object[] args = getArgs(invocation);

        log.info("[dubbo|consumer]-[{}.{}] 耗时:[{}]\n入参:\n{}\n出参:\n {}",
                interfaceClass, methodName, spendTime, JSON.toJSONString(args), JSON.toJSONString(result.getValue()));
    }

    /***
     *  获取 Invocation 参数，过滤掉大参数
     * @author winfun
     * @param invocation invocation
     * @return {@link Object[] }
     **/
    private Object[] getArgs(Invocation invocation) {
        Object[] args = invocation.getArguments();
        args = Arrays.stream(args).filter(arg -> !(arg instanceof byte[]) && !(arg instanceof Byte[]) && !(arg instanceof InputStream) && !(arg instanceof File)).toArray();
        return args;
    }
}

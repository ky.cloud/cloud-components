package com.ky.cloud.components.dubbo.filter;


import com.alibaba.fastjson2.JSON;
import com.ky.cloud.components.context.SystemContext;
import com.ky.cloud.components.context.constant.TraceConstant;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.*;
import org.apache.dubbo.rpc.filter.ExceptionFilter;
import org.slf4j.MDC;

import java.io.File;
import java.io.InputStream;
import java.util.Arrays;


/**
 * DubboRpc服务端过滤器
 *
 * @author chenHui
 * @version 1.0.0
 */
@Slf4j
@Activate(group = CommonConstants.PROVIDER)
public class DubboRpcProviderExceptionFilter extends ExceptionFilter {
    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        return super.invoke(invoker,invocation);
    }


    /**
     *  打印结果
     *
     * @param invocation invocation
     * @param reqAppId 请求系统
     * @since 1.0.0
     */
    private void printLog(Invocation invocation, String reqAppId, String ip, String operator) {
        String interfaceClass = invocation.getInvoker().getInterface().getName();
        String methodName = invocation.getMethodName();
        Object[] args = getArgs(invocation);

        log.info("[dubbo|server]-[{}.{}] 请求系统[{}|{}],操作人员[{}] \n入参:\n{}",
                interfaceClass, methodName,reqAppId,ip,operator, JSON.toJSONString(args[0]));
    }

    /**
     *  获取 Invocation 参数，过滤掉大参数
     *
     * @param invocation invocation
     * @return {@link Object[] }
     * @since 1.0.0
     */
    private Object[] getArgs(Invocation invocation) {
        Object[] args = invocation.getArguments();
        args = Arrays.stream(args).filter(arg -> !(arg instanceof byte[]) && !(arg instanceof Byte[]) && !(arg instanceof InputStream) && !(arg instanceof File)).toArray();
        return args;
    }
}

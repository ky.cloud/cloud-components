package com.ky.cloud.components.dubbo.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 请求结果dto
 * 
 * @author chenHui
 * @version 1.0.0
 */
@Getter
@Setter
public class BaseResultDTO implements Serializable {

    /**
     * 返回结果
     */
    private Boolean resultFlag = true;

    /**
     * 错误信息
     */
    private String errorMsg;

    public void failure(String resultMessage){
        this.setResultFlag(false);
        this.setErrorMsg(resultMessage);
    }
}

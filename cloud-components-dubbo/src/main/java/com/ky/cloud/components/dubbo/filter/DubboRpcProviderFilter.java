package com.ky.cloud.components.dubbo.filter;


import com.alibaba.fastjson2.JSON;
import com.ky.cloud.components.context.SystemContext;
import com.ky.cloud.components.context.constant.TraceConstant;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.*;
import org.slf4j.MDC;

import java.io.File;
import java.io.InputStream;
import java.util.Arrays;


/**
 * DubboRpc服务端过滤器
 *
 * @author chenHui
 * @version 1.0.0
 */
@Slf4j
@Activate(group = CommonConstants.PROVIDER,order = Integer.MIN_VALUE)
public class DubboRpcProviderFilter implements Filter {
    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        try {
            RpcContextAttachment serverAttachment = RpcContext.getServerAttachment();
            String traceId = (String) serverAttachment.getObjectAttachment(TraceConstant.MDC_TRACE_ID);
            String spanId = (String) serverAttachment.getObjectAttachment(TraceConstant.MDC_SPAN_ID);
            String reqAppId = (String) serverAttachment.getObjectAttachment(TraceConstant.REQ_APP);
            String operator = (String) serverAttachment.getObjectAttachment(TraceConstant.OPERATOR);
            String remoteHost = serverAttachment.getRemoteHost();

            SystemContext.initContext(traceId,spanId);
            SystemContext.nextSpan(spanId);
            SystemContext.addOperator(operator);

            MDC.put(TraceConstant.MDC_TRACE_ID, SystemContext.getContext().getTraceId());
            MDC.put(TraceConstant.MDC_SPAN_ID,SystemContext.getContext().getSpanId());

            Result result = invoker.invoke(invocation);
            printLog(invocation, result,reqAppId,remoteHost,operator);
            return result;
        } finally {
            RpcContext.removeContext();
            SystemContext.remove();
        }
    }


    /**
     *  打印结果
     *
     * @param invocation invocation
     * @param result result
     * @param reqAppId 请求系统
     * @since 1.0.0
     */
    private void printLog(Invocation invocation, Result result,String reqAppId,String ip,String operator) {
        String interfaceClass = invocation.getInvoker().getInterface().getName();
        String methodName = invocation.getMethodName();
        Object[] args = getArgs(invocation);

        log.info("[dubbo|server]-[{}.{}] 请求系统[{}|{}],操作人员[{}] \n入参:\n{}\n出参:\n {}",
                interfaceClass, methodName,reqAppId,ip,operator, JSON.toJSONString(args[0]), JSON.toJSONString(result.getValue()));
    }

    /**
     *  获取 Invocation 参数，过滤掉大参数
     *
     * @param invocation invocation
     * @return {@link Object[] }
     * @since 1.0.0
     */
    private Object[] getArgs(Invocation invocation) {
        Object[] args = invocation.getArguments();
        args = Arrays.stream(args).filter(arg -> !(arg instanceof byte[]) && !(arg instanceof Byte[]) && !(arg instanceof InputStream) && !(arg instanceof File)).toArray();
        return args;
    }
}

package com.ky.cloud.components.rocketmq.service;

import com.ky.cloud.components.rocketmq.dto.MQSendDTO;
import com.ky.cloud.components.rocketmq.event.TransactionCommitSendMQEvent;

/**
 * MQ生产者服务接口
 *
 * @author chenHui
 * @version 1.0.0
 */
public interface MQProducerService {

    /**
     * 同步发送
     *
     * @param mqSendDTO MQSendDTO
     * @return boolean true 发送成功 false 发送失败
     * @since 1.0.0
     */
    boolean sendMsg(MQSendDTO mqSendDTO);

    /**
     * 异步发送
     *
     * @param mqSendDTO MQSendDTO
     * @since 1.0.0
     */
    void sendAsyncMsg(MQSendDTO mqSendDTO);

    /**
     * 异步发送
     *
     * @param transactionCommitSendMQEvent TransactionCommitSendMQEvent
     * @since 1.0.0
     */
    void sendAsyncMsg(TransactionCommitSendMQEvent transactionCommitSendMQEvent);
}

package com.ky.cloud.components.rocketmq.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * 事务提交后发生mq事件
 *
 * @author chenHui
 * @version 1.0.0
 */
@Getter
public class TransactionCommitSendMQEvent extends ApplicationEvent {

    private final String topic;

    private final String tag;

    private final String key;

    private final Object msg;

    public TransactionCommitSendMQEvent(Object source, String topic, String tag,String key, Object msg) {
        super(source);
        this.topic = topic;
        this.tag = tag;
        this.key = key;
        this.msg = msg;
    }

}

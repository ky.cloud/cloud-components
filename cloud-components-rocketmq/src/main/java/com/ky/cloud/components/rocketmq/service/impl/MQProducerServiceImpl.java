package com.ky.cloud.components.rocketmq.service.impl;

import com.alibaba.fastjson2.JSON;
import com.ky.cloud.components.context.SystemContext;
import com.ky.cloud.components.rocketmq.dto.MQSendDTO;
import com.ky.cloud.components.rocketmq.event.TransactionCommitSendMQEvent;
import com.ky.cloud.components.rocketmq.service.MQProducerService;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.apache.rocketmq.spring.support.RocketMQHeaders;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * MQ生产者服务接口实现
 *
 * @author chenHui
 * @version 1.0.0
 */
@Slf4j
@Service
public class MQProducerServiceImpl implements MQProducerService {
    @Value("${spring.profiles.active}")
    private String env;

    @Resource
    private RocketMQTemplate rocketMQTemplate;
    @Resource
    private ApplicationEventPublisher applicationEventPublisher;


    /**
     * 获取destination
     */
    private String getDestination(MQSendDTO mqSendDTO) {
        return env + "_" +mqSendDTO.getTopic() + ":" + mqSendDTO.getTag();
    }

    private Message<?> getMessage(MQSendDTO mqSendDTO) {
        if (StringUtils.hasText(mqSendDTO.getKey())) {
            return MessageBuilder.withPayload(JSON.toJSONString(mqSendDTO.getMsg())).setHeader(RocketMQHeaders.KEYS, mqSendDTO.getKey()).build();
        }
        return MessageBuilder.withPayload(JSON.toJSONString(mqSendDTO.getMsg())).build();
    }


    @Override
    public boolean sendMsg(MQSendDTO mqSendDTO) {
        if (mqSendDTO.getMsg() == null) {
            log.error("{},topic:[{}],tag:[{}],key:[{}]", "message is null", mqSendDTO.getTopic(), mqSendDTO.getTag(), mqSendDTO.getKey());
            return false;
        }
        SendResult sendResult = rocketMQTemplate.syncSend(getDestination(mqSendDTO), getMessage(mqSendDTO));
        if (sendResult != null && sendResult.getSendStatus().equals(SendStatus.SEND_OK)) {
            log.info("[mq|producer]-[success] messageId:{}", sendResult.getOffsetMsgId());
            return true;
        }
        log.error("[mq|producer]-[fail] 入参\n:[{}]", JSON.toJSONString(mqSendDTO));
        return false;
    }

    @Override
    public void sendAsyncMsg(MQSendDTO mqSendDTO) {
        if (mqSendDTO.getMsg() == null) {
            log.error("{},topic:[{}],tag:[{}],key:[{}]", "消息体不能为空", mqSendDTO.getTopic(), mqSendDTO.getTag(), mqSendDTO.getKey());
            return;
        }
        SystemContext context = SystemContext.getContext();
        rocketMQTemplate.asyncSend(getDestination(mqSendDTO), getMessage(mqSendDTO), new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                SystemContext.initContext(context.getTraceId(), context.getSpanId());
                log.info("[mq|producer]-[success] messageId:{}", sendResult.getOffsetMsgId());
                SystemContext.remove();
            }

            @Override
            public void onException(Throwable throwable) {
                SystemContext.initContext(context.getTraceId(), context.getSpanId());
                log.error("[mq|producer]-[fail] 入参\n{}\n异常原因:{}", JSON.toJSONString(mqSendDTO), throwable.getMessage(), throwable);
                SystemContext.remove();
            }
        });
    }


    @Override
    public void sendAsyncMsg(TransactionCommitSendMQEvent transactionCommitSendMQEvent) {
        applicationEventPublisher.publishEvent(transactionCommitSendMQEvent);
    }
}

package com.ky.cloud.components.rocketmq.listener;

import com.ky.cloud.components.rocketmq.dto.MQSendDTO;
import com.ky.cloud.components.rocketmq.event.TransactionCommitSendMQEvent;
import com.ky.cloud.components.rocketmq.service.MQProducerService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 事务提交监听器
 *
 * @author chenHui
 * @version  1.0.0
 **/
@Component
public class TransactionCommitSendMQListener {

    @Resource
    private MQProducerService mqProducerService;


    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
    public void send(TransactionCommitSendMQEvent event) {
        MQSendDTO mqSendDTO = new MQSendDTO();
        mqSendDTO.setTopic(event.getTopic());
        mqSendDTO.setTag(event.getTag());
        mqSendDTO.setKey(event.getKey());
        mqSendDTO.setMsg(event.getMsg());
        mqProducerService.sendAsyncMsg(mqSendDTO);
    }
}

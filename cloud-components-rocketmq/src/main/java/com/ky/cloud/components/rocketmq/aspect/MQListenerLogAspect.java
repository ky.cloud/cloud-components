package com.ky.cloud.components.rocketmq.aspect;

import com.ky.cloud.components.context.SystemContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;


/**
 * mq监听者日志切面
 *
 * @author chenHui
 * @version 1.0.0
 */
@Aspect
@Component
@Slf4j
public class MQListenerLogAspect {

    @Value("${spring.profiles.active}")
    private String env;
    @Value("${rocketmq.producer.group}")
    private String group;

    @Pointcut("@within(org.apache.rocketmq.spring.annotation.RocketMQMessageListener)")
    public void logAnnotation() {
    }

    @Around("logAnnotation()")
    public Object mqLogAround(ProceedingJoinPoint point) {
        // 获取实现类上的RocketMQMessageListener注解
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();
        Class<?> aClass = method.getDeclaringClass();
        RocketMQMessageListener annotation = aClass.getAnnotation(RocketMQMessageListener.class);
        String topic = annotation.topic();
        topic = topic.replace("${spring.profiles.active}", env).replace("${rocketmq.producer.group}", group);
        String tag = annotation.selectorExpression();
        Object[] args = point.getArgs();
        MessageExt messageExt = (MessageExt) args[0];
        byte[] body = messageExt.getBody();
        SystemContext.initTraceMdc();
        log.info("[mq|consume]-[|] topic:[{}]，tag:[{}]，messageId:[{}]", topic, tag, messageExt.getMsgId());
        Object object = null;
        try {
            object = point.proceed();
            log.info("[mq|consume]-[|success]");
        } catch (Throwable e) {
            log.error("[mq|consume]-[|fail],message:{} \n",new String(body), e);
        } finally {
            SystemContext.remove();
        }
        return object;
    }
}

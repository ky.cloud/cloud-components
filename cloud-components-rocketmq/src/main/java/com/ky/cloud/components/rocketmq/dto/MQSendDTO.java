package com.ky.cloud.components.rocketmq.dto;

/**
 * mq推送dto
 *
 * @author chenHui
 * @version 1.0.0
 */
public class MQSendDTO {
    /**
     * mqTopic
     */
    private String topic;
    /**
     * mqTag
     */
    private String tag;
    /**
     * mqKey
     */
    private String key;
    /**
     * 消息体
     */
    private Object msg;


    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getMsg() {
        return msg;
    }

    public void setMsg(Object msg) {
        this.msg = msg;
    }
}

package com.ky.cloud.components.batch.aop;

import com.ky.cloud.components.context.SystemContext;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import tech.powerjob.worker.core.processor.TaskContext;

/**
 * @author chenhui
 * @version 1.0.0
 */
@Slf4j
@Aspect
@Component
public class BasicProcessorLogAspect {

    @Pointcut("execution(* tech.powerjob.worker.core.processor.sdk.BasicProcessor.process())")
    public void pointcut(){}

    @Before(value = "pointcut()")
    public void before(JoinPoint joinPoint){
        Object[] args = joinPoint.getArgs();
        TaskContext taskContext = (TaskContext) args[0];
        SystemContext.initContext(taskContext.getInstanceId().toString(),"0");
        SystemContext.initTraceMdc();
        log.info("[batch|BasicProcessor],task processing begins execution");
    }

    @After(value = "pointcut()")
    public void after(){
        SystemContext.remove();
    }

}

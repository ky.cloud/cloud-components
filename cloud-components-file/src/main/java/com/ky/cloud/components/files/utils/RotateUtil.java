package com.ky.cloud.components.files.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Base64Utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.UUID;

/**
 * 图片旋转工具类
 *
 * @author chz
 * @version  1.0.0
 */
@Slf4j
public class RotateUtil {

    /**
     * base64图片旋转
     *
     * @author chz
     * @param base64 图片base64串
     * @param angel 旋转角度
     * @return 旋转后图片base64串
     * @since 1.0.0
     */
    public static String rotateBase64(String base64, int angel){
        try {
            File targetFile = File.createTempFile(UUID.randomUUID().toString(), ".png");
            OutputStream out = new FileOutputStream(targetFile);
            byte[] b = Base64Utils.decodeFromString(base64);
            for (int i = 0; i < b.length; i++) {
                if (b[i] < 0) {
                    b[i] += 256;
                }
            }
            out.write(b);
            out.flush();
            BufferedImage bufferImage = ImageIO.read(targetFile);
            BufferedImage rbuffer = rotateImage(bufferImage, angel);
            File newFile = File.createTempFile(UUID.randomUUID().toString(), ".png");
            ImageIO.write(rbuffer, "png", newFile);
            String imageBase64 = GetImageStr(newFile);
            //去除旋转签名串中的多余特殊字符
            imageBase64 = imageBase64.replace("\r","");
            imageBase64 = imageBase64.replace("\n","");
            //删除临时文件
            if (targetFile.exists()){
                targetFile.delete();
            }
            if (newFile.exists()){
                newFile.delete();
            }
            return imageBase64;
        }catch (Exception e){
            log.info("base64图片旋转异常："+e.getMessage());
            return null;
        }
    }

    /**
     * 文件Base64编码
     *
     * @author chz
     * @param imgFilePath 文件对象
     * @return base64字符串
     * @since 1.0.0
     */
    public static String GetImageStr(File imgFilePath) {
        byte[] data = null;
        // 读取图片字节数组
        try {
            InputStream in = new FileInputStream(imgFilePath);
            data = new byte[in.available()];
            in.read(data);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (data ==null){
            return null;
        }
        // 返回Base64编码过的字节数组字符串
        return Base64Utils.encodeToString(data);
    }

    /**
     * 图片旋转
     *
     * @author chz
     * @param bufferedimage 图片对象
     * @param degree 旋转角度
     * @return 旋转后图片
     * @since 1.0.0
     */
    public static BufferedImage rotateImage( BufferedImage bufferedimage,
                                             int degree) {
        //原始图象的宽度
        int iw = bufferedimage.getWidth();
        //原始图象的高度
        int ih = bufferedimage.getHeight();
        int w = 0;
        int h = 0;
        int x;
        int y;
        degree = degree % 360;
        if (degree < 0) {
            //将角度转换到0-360度之间
            degree = 360 + degree;
        }
        /**
         *确定旋转后的图象的高度和宽度
         */
        if (degree == 180 || degree == 0 || degree == 360) {
            w = iw;
            h = ih;
        } else if (degree == 90 || degree == 270) {
            w = ih;
            h = iw;
        }
        //确定原点坐标
        x = (w / 2) - (iw / 2);
        y = (h / 2) - (ih / 2);
        int type = bufferedimage.getColorModel().getTransparency();
        BufferedImage img;
        Graphics2D graphics2d;
        (graphics2d = (img = new BufferedImage(w,h, type))
                .createGraphics()).setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2d.rotate(Math.toRadians(degree), (double) w /2, (double) h /2);
        graphics2d.translate(x, y);
        graphics2d.drawImage(bufferedimage, 0, 0, null);
        graphics2d.dispose();
        return img;
    }
}

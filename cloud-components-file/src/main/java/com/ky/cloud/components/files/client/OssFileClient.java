package com.ky.cloud.components.files.client;

import com.aliyun.oss.ClientConfiguration;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.common.auth.CredentialsProvider;
import com.aliyun.oss.common.auth.DefaultCredentialProvider;
import com.ky.cloud.components.files.config.FileConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * @author chenHui
 * @version 1.0.0
 */
@Slf4j
@Component
@ConditionalOnProperty(name = "file.store.clientType", havingValue = "oss")
public class OssFileClient implements FileStoreClient {

    private OSSClient ossClient;

    public OssFileClient(FileConfig config) {
        ClientConfiguration conf = new ClientConfiguration();
        conf.setIdleConnectionTime(config.getIdleConnectionTime());
        conf.setConnectionTimeout(config.getConnectionTimeout());
        conf.setConnectionRequestTimeout(config.getConnectionRequestTimeout());
        conf.setRequestTimeout(config.getRequestTimeout());
        conf.setSocketTimeout(config.getSocketTimeout());
        CredentialsProvider credentialsProvider = new DefaultCredentialProvider(config.getAccessKeyId(), config.getSecretAccessKey());
        this.ossClient = new OSSClient(config.getServerAddress(), credentialsProvider,conf);
        log.info("ossClient---连接成功...");
    }
    @Override
    public boolean uploadFile(byte[] data, String storagePath,String filename) {
        return true;
    }

    @Override
    public boolean uploadFile(File localFile, String storageDir) {
        return true;
    }

    @Override
    public boolean downloadFile(String storagePath, String downloadFilePath) {
        return false;
    }

    @Override
    public String getPreviewUrl(String filepath) {
        return null;
    }
}

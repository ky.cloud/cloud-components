package com.ky.cloud.components.files.exception;

/**
 * 文件存储异常
 *
 * @author chenHui
 * @version 1.0.0
 */
public class FileStorageException extends RuntimeException{

    public FileStorageException() {
        super();
    }

    public FileStorageException(String message) {
        super(message);
    }
}

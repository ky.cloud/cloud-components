package com.ky.cloud.components.files;


import com.ky.cloud.components.files.client.CosFileClient;
import com.ky.cloud.components.files.client.FileStoreClient;
import com.ky.cloud.components.files.client.OssFileClient;
import com.ky.cloud.components.files.config.FileConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author chenHui
 * @version 1.0.0
 */
@Slf4j
@Configuration
public class FileStoreClientFactory {
    
    @Bean
    public FileStoreClient createFileClient(FileConfig fileConfig){
        if ("cos".equals(fileConfig.getClientType())){
            log.info("-------------正在装配cssFileClient---------------------");
            return new CosFileClient(fileConfig);
        }else if ("oss".equals(fileConfig.getClientType())){
            log.info("-------------正在装配ossFileClient---------------------");
            return new OssFileClient(fileConfig);
        } else {
            throw new BeanCreationException("fileStoreClient创建异常,file.store.clientType="+fileConfig.getClientType());
        }
    }
}

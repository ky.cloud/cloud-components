package com.ky.cloud.components.files.client;

import java.io.File;

/**
 * 接口
 *
 * @author chenHui
 * @version 1.0.0
 */
public interface FileStoreClient {
    /**
     * 文件上传
     *
     * @param data 文件流
     * @param storagePath 存储路径
     * @return String 对象存储路径
     * @since 1.0.0
     */
    boolean uploadFile(byte[] data, String storagePath,String filename);
    /**
     * 文件上传
     *
     * @param localFile 本地文件
     * @param storagePath 存储路径
     * @return boolean
     * @since 1.0.0
     */
    boolean uploadFile(File localFile, String storagePath);
    /**
     * 文件下载
     *
     * @param storagePath 对象存储路径
     * @param downloadFilePath 下载文件路径
     * @return String 对象存储路径
     * @since 1.0.0
     */
    public boolean downloadFile(String storagePath, String downloadFilePath);

    /**
     * 获取预览url
     *
     * @param storagePath 对象存储路径
     * @return 预览url
     * @since 1.0.0
     */
    String getPreviewUrl(String storagePath);
}

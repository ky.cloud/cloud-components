package com.ky.cloud.components.files.client;

import com.ky.cloud.components.files.config.FileConfig;
import com.ky.cloud.components.files.exception.FileStorageException;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.exception.CosClientException;
import com.qcloud.cos.exception.CosServiceException;
import com.qcloud.cos.http.HttpMethodName;
import com.qcloud.cos.model.*;
import com.qcloud.cos.region.Region;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;

/**
 * @author chenHui
 * @version 1.0.0
 */
@Slf4j
@Component
@ConditionalOnProperty(name = "file.store.clientType", havingValue = "cos")
public class CosFileClient implements FileStoreClient {
    private final String bucketName;
    private final COSClient cosClient;

    public CosFileClient(FileConfig config) {
        COSCredentials cred = new BasicCOSCredentials(config.getAccessKeyId(), config.getSecretAccessKey());
        Region region = new Region(config.getServerAddress());
        ClientConfig clientConfig = new ClientConfig(region);
        clientConfig.setConnectionTimeout(config.getConnectionTimeout());
        clientConfig.setConnectionRequestTimeout(config.getConnectionRequestTimeout());
        this.cosClient = new COSClient(cred, clientConfig);
        bucketName = config.getBucket();
        log.info("cosClient---连接成功...");
    }

    @Override
    public boolean uploadFile(byte[] data, String storagePath,String filenam) {
        try {
            InputStream input = new ByteArrayInputStream(data);
            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentLength(data.length);
            cosClient.putObject(bucketName, storagePath, input, objectMetadata);
            input.close();
        } catch (IOException | CosClientException e) {
            log.error("上传文件异常", e);
            return false;
        }
        return true;
    }

    @Override
    public boolean uploadFile(File localFile, String storageDir) {
        if (!localFile.exists()) {
            log.error("COS待上传文件[{}]不存在", localFile.getPath());
            throw new FileStorageException("上传文件不存在");
        }
        try {
            log.info("文件[{}]开始上传腾讯云,上传腾讯云路径[{}]", localFile.getPath(), storageDir);
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, storageDir, localFile);
            // 设置存储类型,默认是标准(Standard), 低频(standard_ia)
            putObjectRequest.setStorageClass(StorageClass.Standard);
            cosClient.putObject(putObjectRequest);
            cosClient.shutdown();
        } catch (CosServiceException e) {
            log.error("上传文件平台Server异常", e);
            return false;
        } catch (CosClientException e) {
            log.error("上传文件平台Client异常", e);
            return false;
        }
        localFile.delete();
        return true;
    }

    @Override
    public boolean downloadFile(String filePath, String downloadFilePath) {
        try {
            File downFile = new File(downloadFilePath);
            GetObjectRequest getObjectRequest = new GetObjectRequest(bucketName, filePath);
            this.cosClient.getObject(getObjectRequest, downFile);
            return true;
        } catch (CosClientException var7) {
            var7.printStackTrace();
            return false;
        }
    }

    @Override
    public String getPreviewUrl(String filepath) {
        // 设置签名过期时间(可选), 若未进行设置则默认使用 ClientConfig 中的签名过期时间(1小时)
        // 这里设置签名在半个小时后过期
        Date expirationDate = new Date(System.currentTimeMillis() + 30 * 60 * 1000);
        // 请求的 HTTP 方法，上传请求用 PUT，下载请求用 GET，删除请求用 DELETE
        HttpMethodName method = HttpMethodName.GET;
        URL url = cosClient.generatePresignedUrl(bucketName, filepath, expirationDate, method);
        // 确认本进程不再使用 cosClient 实例之后关闭
        this.cosClient.shutdown();
        String netUrl = url.toString();
        String[] split = netUrl.split("//?");
        return split[0];
    }
}
